import image.mapping.Image;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TesteImage {

    public static void main(String[] args) throws IOException {

        System.out.println("Distancia e Similaridade");
        String path;
        path = ImageConverter.convertTiffAndCrop("imagens/baixados.jpg");
        System.out.println(path);

        // Criar a instância da imagem de referência após a conversão
        Image imageReferencia = new Image(Image.componentExtract(path), 255);

        File[] arquivos = new File("imagens").listFiles();

        ArrayList<String> tiffFiles = new ArrayList<>();

        for (File arquivo : arquivos) {
            if (arquivo.isFile() && arquivo.getName().endsWith(".jpg")) {
                ImageConverter.convertTiffAndCrop(String.valueOf(arquivo));
                String nomeTiff = arquivo.getName().replace(".jpg", ".tiff");
                String caminhoTiff = arquivo.getParent() + File.separator + nomeTiff;
                tiffFiles.add(caminhoTiff);
            }
        }

        // Lista para armazenar os resultados das métricas
        List<ImageMetricResult> results = new ArrayList<>();

        for (String tiffFile : tiffFiles) {
            Image imageTiff = new Image(Image.componentExtract(tiffFile), 255);

            double distanciaEuclidiana = imageReferencia.compareEuclideanDistance(imageTiff);
            double similaridadeCoseno = imageReferencia.compareCosineSimilarity(imageTiff);

            // Armazenar o resultado das métricas em uma lista
            results.add(new ImageMetricResult(tiffFile, distanciaEuclidiana, similaridadeCoseno));
        }

        // Ordenar a lista de resultados com base na distância euclidiana
        Collections.sort(results, Comparator.comparingDouble(ImageMetricResult::getDistanciaEuclidiana));

        // Imprimir os resultados ordenados pela distância euclidiana
        System.out.println("Resultados ordenados pela Distância Euclidiana:");
        for (ImageMetricResult result : results) {
            System.out.printf("Distância Euclidiana: %s -> %s: %.2f\n",
                    path, result.getFileName(), result.getDistanciaEuclidiana());
        }

        // Ordenar a lista de resultados com base na similaridade do cosseno
        Collections.sort(results, Comparator.comparingDouble(ImageMetricResult::getSimilaridadeCoseno).reversed());

        // Imprimir os resultados ordenados pela similaridade do cosseno
        System.out.println("\nResultados ordenados pela Similaridade Cosseno:");
        for (ImageMetricResult result : results) {
            System.out.printf("Similaridade Cosseno: %s -> %s: %.2f%%\n",
                    path, result.getFileName(), (result.getSimilaridadeCoseno() * 100));
        }
    }

    private static class ImageMetricResult {
        private String fileName;
        private double distanciaEuclidiana;
        private double similaridadeCoseno;

        public ImageMetricResult(String fileName, double distanciaEuclidiana, double similaridadeCoseno) {
            this.fileName = fileName;
            this.distanciaEuclidiana = distanciaEuclidiana;
            this.similaridadeCoseno = similaridadeCoseno;
        }

        public String getFileName() {
            return fileName;
        }

        public double getDistanciaEuclidiana() {
            return distanciaEuclidiana;
        }

        public double getSimilaridadeCoseno() {
            return similaridadeCoseno;
        }
    }
}
