import java.io.IOException;

import image.mapping.Image;

public class Core {

	public static void main(String[] args) throws IOException {

		String filename01 = "images/skin.tif";
		String filename02 = "images/converter/teste1.tiff";
		
		Image image01 = null;
		Image image02 = null;
		
		//tarefa0 - cliente
		String input = "images/converter/teste1.jpg";
		ImageConverter.convertTiff(input);//notifica a thread que vai calcular o histograma
		
		//tarefa1 - servidor (cliente usa via stub)
		image01 = new Image(Image.componentExtract(filename01), 255);
		image02 = new Image(Image.componentExtract(filename02), 255);
					
		//tarefa2 - servidor (cliente usa via stub)
		double similarityCosine = image01.compareCosineSimilarity(image02);
		System.out.printf("\nSIMILARIDADE COSENO: %.2f%%\n", (similarityCosine * 100));

		//tarefa3
		TesteHistogram.printHistogramFromFile("images/converter/teste1.tiff");//esta thread fique em estado de espera enquanto a foto � convertida
		

	}

}
